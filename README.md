Le conteneur leger de spring

Pour activer l'annotation en spring: ça se fait dans le fichier ApplicationContextXml au niveau de l'entete dans la balise beans. (context).

@Component a comme filles:  @repository @service @controller

On scanne jamais la racine du projet exple : com.mycompany.invoise mais plutot le type :com.mycompany.invoise.controller.web

On peut se passe du fichier ApplicationContextXml ent mettant les classes charges dans la classe Main (App)

{ @Configuration
@ComponentScan(basePackages = {"com.mycompany.invoise.controller.web","com.mycompany.invoise.service.prefix","com.mycompany.invoise.repository.database"})
@PropertySource("classpath:application.properties")
public class App }
 